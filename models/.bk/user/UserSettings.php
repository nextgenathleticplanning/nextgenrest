<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "userSettings".
 *
 * @property int $id
 * @property int $user_id
 * @property int $key
 * @property string $data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userSettings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'key', 'data', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'key', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['user_id', 'key'], 'unique', 'targetAttribute' => ['user_id', 'key']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'key' => 'Key',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
