<?php

namespace app\models\user;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use app\models\helpers\Permissions;
use app\models\helpers\UserHelpers;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property int $status
 * @property int $role
 * @property string $auth_key
 * @property string $access_token
 * @property string $password_hash
 * @property string $confirmation_token
 * @property int $confirmation_sent_at
 * @property int $confirmed_at
 * @property string $recovery_token
 * @property int $recovery_sent_at
 * @property int $blocked_at
 * @property string $registration_ip
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Profile $profile
 * @property Session[] $sessions
 * @property SocialAccounts[] $socialAccounts
 * @property Role $role0
 * @property Status $status0
 * @property UserSettings[] $userSettings
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 20;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'auth_key', 'access_token', 'confirmation_token'], 'required'],
            [['status', 'role', 'confirmation_sent_at', 'confirmed_at', 'recovery_sent_at', 'blocked_at', 'created_at', 'updated_at'], 'integer'],
            [['username', 'email', 'password_hash', 'registration_ip'], 'string', 'max' => 255],
            [['auth_key', 'access_token', 'confirmation_token', 'recovery_token'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['auth_key'], 'unique'],
            [['access_token'], 'unique'],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['auth_key'], 'unique'],
            [['access_token'], 'unique'],
            [['role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status' => 'id']],
            ['rememberMe', 'boolean'],
            [['password','userProfile','userSocialAccount','rememberMe'],'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'status' => 'Status',
            'role' => 'Role',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'password_hash' => 'Password Hash',
            'confirmation_token' => 'Confirmation Token',
            'confirmation_sent_at' => 'Confirmation Sent At',
            'confirmed_at' => 'Confirmed At',
            'recovery_token' => 'Recovery Token',
            'recovery_sent_at' => 'Recovery Sent At',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessions()
    {
        return $this->hasMany(Session::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccounts::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings()
    {
        return $this->hasMany(UserSettings::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()->where(['access_token' => $token,'status' => self::STATUS_ACTIVE])->one();
    }

    /**
    * Returns an ID that can uniquely identify a user identity.
    * @return string|int an ID that uniquely identifies a user identity.
    */
    /**
    * @inheritdoc
    */
    public function getId()
    {
       return $this->getPrimaryKey();
    }

    /**
    * Returns a key that can be used to check the validity of a given identity ID.
    *
    * The key should be unique for each individual user, and should be persistent
    * so that it can be used to check the validity of the user identity.
    *
    * The space of such keys should be big enough to defeat potential identity attacks.
    *
    * This is required if [[User::enableAutoLogin]] is enabled.
    * @return string a key that is used to check the validity of a given identity ID.
    * @see validateAuthKey()
    */
    /**
    * @inheritdoc
    */
    public function getAuthKey()
    {
       return $this->auth_key;
    }

    /**
    * Validates the given auth key.
    *
    * This is required if [[User::enableAutoLogin]] is enabled.
    * @param string $authKey the given auth key
    * @return bool whether the given auth key is valid.
    * @see getAuthKey()
    */
    /**
    * @inheritdoc
    */
    public function validateAuthKey($authKey)
    {
       return $this->getAuthKey() === $authKey;
    }

    public function getFullName(){
        return $this->profile->first_name . ' '. $this->profile->last_name;
    }

    public function getAddress(){
        return $this->profile->street . ' ' . $this->profile->city . ' ' . $this->profile->state . ', ' . $this->profile->zip;
    }


    public function getMailer(){
        // $sender = (empty(Yii::$app->systemData->companyInfo->email))?Yii::$app->params['adminEmail']:Yii::$app->systemData->companyInfo->email;
        // return \Yii::createObject([
        //     'class' => Mailer::className(),
        //     'viewPath' => Yii::$app->getModule('user')->mailerViewPath,
        //     'sender' => $sender,
        // ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($model)
    {
        $timestamp = $model->recovery_sent_at;
        if (empty($timestamp)) {
            return false;
        }
        return ($timestamp + $this->module->getRecoverWithin() < time());
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function hashPassword()
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $var  = Yii::$app->security->generateRandomString();
        $var_exists = $this->find()->where(['auth_key' => $var])->exists();
        if($var_exists){
            //Regenerate Random String until Unique One Is Found.
            while ($this->find()->where(['auth_key' => $var])->exists()){
                $var = Yii::$app->security->generateRandomString();
            }
        }
        $this->auth_key = $var;
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordRecoveryToken($uid,$token)
    {
        $model =  static::findOne([
            'recovery_token' => $token,
            'status' =>  \app\models\helpers\UserHelpers::getStatusId(User::STATUS_ACTIVE),
            'auth_key' => $uid
        ]);

        if($model == null){
            Yii::$app->session->setFlash('error', 'Token has expired or already been used. If you need to reset your password please create another recovery token.');
            return null;
        }
        if(static::isPasswordResetTokenValid($model)){
            Yii::$app->session->setFlash('error', 'Recovery Token has expired, Please resubmit Password Reset.');
            return null;
        }
        return $model;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->andWhere(
                [
                    'or',
                    ['username' => $username],
                    ['email' => $username]
                ]
            )
            ->one();
    }


    /**
     * Generates API authentication key
     */
    public function generateAccessToken()
    {
        $var  = Yii::$app->security->generateRandomString();
        $var_exists = $this->find()->where(['access_token' => $var])->exists();
        if($var_exists){
            //Regenerate Random String until Unique One Is Found.
            while ($this->find()->where(['access_token' => $var])->exists()){
                $var = Yii::$app->security->generateRandomString();
            }
        }
        $this->access_token = $var;
    }

    /**
     * Generates API authentication key
     */
    public function generateConfirmationToken()
    {
        $var  = Yii::$app->security->generateRandomString();
        $var_exists = $this->find()->where(['confirmation_token' => $var])->exists();
        if($var_exists){
            //Regenerate Random String until Unique One Is Found.
            while ($this->find()->where(['confirmation_token' => $var])->exists()){
                $var = Yii::$app->security->generateRandomString();
            }
        }
        $this->confirmation_token = $var;
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {

        $var  = Yii::$app->security->generateRandomString();
        $var_exists = $this->find()->where(['recovery_token' => $var])->exists();
        if($var_exists){
            //Regenerate Random String until Unique One Is Found.
            while ($this->find()->where(['recovery_token' => $var])->exists()){
                $var = Yii::$app->security->generateRandomString();
            }
        }
        $this->recovery_token = $var;
        $this->recovery_sent_at = time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->recovery_token = null;
        $this->recovery_sent_at = null;
    }

    /**
     * Generates new username based on email address, or creates new username
     * like "emailuser1".
     */
    public function generateUsername($email)
    {
        // try to use name part of email
        $username = explode('@', $email)[0];

        $username_exists = $this->find()->where(['username' => $username])->exists();
        if(!$username_exists){
            return $username;
        }
        $i=0;
        // generate username like "user1", "user2", etc...
        while ($this->find()->where(['username' => $username])->exists()){
            $username = $username . ++$i;
        }
        return $username;
    }

    public function setEnableConfirmation($bool = true){
        $this->enableConfirmation = $bool;
    }


    protected function setEnableGeneratingPassword($bool = false){
        $this->_enableGeneratingPassword = $bool;
    }

    public function getEnableGeneratingPassword(){
        return $this->_enableGeneratingPassword;
    }

    public function confirmAccount(){
        $this->confirmed_at = time();
        return $this->save();
    }
    public function blockAccount($block = true){
        $this->blocked_at = ($block)?time():null;
        return $this->update();
    }
    public function activate($activate = true){
        $this->status = ($activate)?UserHelpers::getStatusId(static::STATUS_ACTIVE):UserHelpers::getStatusId(static::STATUS_INACTIVE);
        return $this->update();
    }
    public function setIpAddress(){
        $this->registration_ip = (is_a(Yii::$app,'yii\web\Application'))? Yii::$app->request->userIp: null;
    }
    public function getIsActive(){
        return ($this->status == UserHelpers::getStatusId(static::STATUS_ACTIVE));
    }

    public function createAccount(){
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();
        try {
            if($this->password  == null){
                $this->setEnableGeneratingPassword(true);
                $this->password = Yii::$app->security->generateRandomString(8);
            }
            $time = time();
            $this->username = ($this->username == null) ? $this->generateUsername($this->email) : $this->username;
            $this->generateConfirmationToken();
            $this->setIpAddress();
            $this->generateAuthKey();
            $this->generateAccessToken();
            // $this->hashPassword();
            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }
            if ($this->enableConfirmation) {
                if( !$this->mailer->sendConfirmationMessage($this)){
                    $transaction->rollBack();
                    return false;
                }
                $this->confirmation_sent_at = $time;
                if (!$this->update()) {
                    $transaction->rollBack();
                    return false;
                }
            }else{
                if($this->getEnableGeneratingPassword()){
                    $this->generatePasswordResetToken();
                }
                if(!$this->mailer->sendWelcomeMessage($this)){
                    $transaction->rollBack();
                    return false;
                }
                $this->confirmed_at = $time;
                $this->confirmation_sent_at = $time;
                if (!$this->update()) {
                    $transaction->rollBack();
                    return false;
                }
            }
            $transaction->commit();
            return $this;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::trace($e->getMessage(),'dev');
            throw $e;
        }

    }

    public function resendConfirmation(){
        if ($this->getIsNewRecord() == true) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on non-existing user');
        }
        $transaction = $this->getDb()->beginTransaction();
        try {
            $this->confirmation_sent_at = time();
            if(!$this->mailer->sendConfirmationMessage($this) || !$this->update()){
                $transaction->rollBack();
                return false;
            }
            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::trace($e->getMessage(),'dev');
            throw $e;
        }

    }

    public function resetPassword(){
        if ($this->getIsNewRecord() == true) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on non-existing user');
        }

        $transaction = $this->getDb()->beginTransaction();
        try {
            $this->generatePasswordResetToken();
            if(!$this->mailer->sendPasswordResetMessage($this) || !$this->update()){
                $transaction->rollBack();
                return false;
            }
            $transaction->commit();
            return $this;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::trace($e->getMessage(),'dev');
            throw $e;
        }
    }


    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {

        }
        if (!empty($this->password)) {
            $this->hashPassword();
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $_profile = \Yii::createObject([
                'class' => Profile::className(),
                'first_name' => ArrayHelper::getValue($this->userProfile,'first_name'),
                'middle_name' => ArrayHelper::getValue($this->userProfile,'middle_name'),
                'last_name' => ArrayHelper::getValue($this->userProfile,'last_name'),
                'birth_date' =>ArrayHelper::getValue($this->userProfile,'birth_date'),
                'birth_month' =>ArrayHelper::getValue($this->userProfile,'birth_month'),
                'birth_day' =>ArrayHelper::getValue($this->userProfile,'birth_day'),
                'birth_year' =>ArrayHelper::getValue($this->userProfile,'birth_year'),
                'gender' => ArrayHelper::getValue($this->userProfile,'gender'),
                'alternate_email' => ArrayHelper::getValue($this->userProfile,'alternate_email'),
                'website' => ArrayHelper::getValue($this->userProfile,'website'),
                'street' => ArrayHelper::getValue($this->userProfile,'street'),
                'city' => ArrayHelper::getValue($this->userProfile,'city'),
                'state' => ArrayHelper::getValue($this->userProfile,'state'),
                'zip' => ArrayHelper::getValue($this->userProfile,'zip'),
                'phone' => ArrayHelper::getValue($this->userProfile,'phone'),
            ]);
            $_profile->link('user', $this);
        }
    }

    public static function restLogin($username, $password){
        // Yii::trace($username,$password,'dev');
        // username, password are mandatory fields
        if(empty($username) || empty($password)){
            return null;
        }

        $user = static::find()->andWhere(['username' => $username,,'status' => self::STATUS_ACTIVE])->one();
        // if no record matching the requested user
        if(empty($user)){
            return null;
        }
        // hashed password from user record
        $user_password_hash = $user->password_hash;
        // validate password
        $isPass = Yii::$app->security->validatePassword($password, $user_password_hash);
        // if password validation fails
        if(!$isPass){
            return null;
        }
        // if user validates (both user_email, user_password are valid)
        return $user;
    }

}
