<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $birth_date
 * @property int $gender
 * @property string $backup_email
 * @property string $street
 * @property string $city
 * @property string $state
 * @property int $zip
 * @property string $phone
 * @property int $email_prefs
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{

    const ALL_EMAILS = 100;
    const NO_EMAILS  = 0;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'gender', 'zip', 'email_prefs', 'created_at', 'updated_at'], 'integer'],
            [['birth_date'], 'safe'],
            [['first_name', 'middle_name', 'last_name', 'backup_email', 'street', 'city', 'state', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'birth_date' => 'Birth Date',
            'gender' => 'Gender',
            'backup_email' => 'Backup Email',
            'street' => 'Street',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'phone' => 'Phone',
            'email_prefs' => 'Email Prefs',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
