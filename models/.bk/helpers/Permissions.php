<?php

namespace app\models\helpers;

use app\models\helpers\UserHelpers;
use Yii;
use yii\base\Model;

class Permissions extends Model
{
	const ADMIN_ROLE = 'Admin';
	const SUPERADMIN_ROLE = 'SuperUser';
	const STANDARD_ROLE = 'Standard';

	public static function getRole($role){
		return UserHelpers::getRoleId($role) ;
	}
	public static function isSuperAdminUser($role = false){
		if(!$role){
			$role = Yii::$app->user->identity->role;
		}
		return  (UserHelpers::getRoleId(self::SUPERADMIN_ROLE) == $role);
	}

	public static function isAdminUser($role = false){
		if(!$role){
			$role = Yii::$app->user->identity->role;
		}
		return (UserHelpers::getRoleId(self::ADMIN_ROLE)==$role);
	}


	public static  function isMinRequiredRole($minRole ,$role = false){
		if(!$role){
			$role = Yii::$app->user->identity->role;
		}
		return ( $role >= UserHelpers::getRoleId($minRole) );
	}

	public static function isSameuser($id){
		return ($id == Yii::$app->user->identity->id);
	}

	public static function isAdminApp(){
		return (Yii::$app->getModule('pyrocms')->appId == Yii::$app->id);
	}

}
