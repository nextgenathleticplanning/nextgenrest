<?php
namespace app\models\auth;

use yii\di\Instance;
use yii\filters\auth\AuthMethod;
use yii\filters\auth\HttpBearerAuth as Model;
use Yii;
use Auth0\SDK\JWTVerifier;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\UnauthorizedHttpException;

class Auth0HttpBearerAuth extends Model
{
    protected $token;
    protected $tokenInfo;

    /**
     * {@inheritdoc}
     */
    public $header = 'Authorization';
    /**
     * {@inheritdoc}
     */
    public $pattern = '/^Bearer\s+(.*?)$/';
    /**
     * @var string the HTTP authentication realm
     */
    public $realm = 'api';
    /**
     * {@inheritdoc}
     */
    public function challenge($response)
    {
        $response->getHeaders()->set('WWW-Authenticate', "Bearer realm=\"{$this->realm}\"");
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate($user, $request, $response)
    {
        $authHeader = $request->getHeaders()->get($this->header);
        // Yii::trace($authHeader,'dev');
        if ($authHeader !== null) {
            if ($this->pattern !== null) {
                if (preg_match($this->pattern, $authHeader, $matches)) {
                    $token = $matches[1];
                } else {
                    return null;
                }
            }
            //Use Auth0 To Verify Token
            if(!$this->setCurrentToken($token)){
                return null;
            }
            $identity = $user->loginByAccessToken($this->tokenInfo->sub);
            if ($identity === null) {
                $this->challenge($response);
                $this->handleFailure($response);
            }
            return $identity;
        }
        return null;
    }

    public function getUserData(){
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://pyrotechsolutions.auth0.com/userinfo')
            ->addHeaders(['Authorization' => 'Bearer '.$this->token])
            ->send();
        return  ($response->isOk)? $response->data:null;
    }


    public function setCurrentToken($token) {
        try {
            $verifier = new JWTVerifier([
                'supported_algs' => ['RS256'],
                'valid_audiences' => ['https://pyrotechsolutions.auth0.com/api/v2/'],
                'authorized_iss' => ['https://pyrotechsolutions.auth0.com/']
            ]);
            $this->token = $token;
            $this->tokenInfo = $verifier->verifyAndDecode($token);
            return true;
        } catch(\Auth0\SDK\Exception\CoreException $e) {
            throw new UnauthorizedHttpException('Auth0 '.$e->getMessage());
        }
    }

    public function checkScope($scope){
        if ($this->tokenInfo){
            $scopes = explode(" ", $this->tokenInfo->scope);
            foreach ($scopes as $s){
                if ($s === $scope)
                return true;
            }
        }
        return false;
    }
}
