<?php
namespace app\models\auth;


use Yii;
use yii\di\Instance;
use yii\filters\auth\AuthMethod;
use app\models\auth\TokenJwt;
use yii\filters\auth\HttpBearerAuth as Model;
use app\models\user\AuthUser;

class TokenHttpBearerAuth extends Model
{

    /**
     * {@inheritdoc}
     */
    public $header = 'Authorization';
    /**
     * {@inheritdoc}
     */
    public $pattern = '/^Bearer\s+(.*?)$/';
    /**
     * @var string the HTTP authentication realm
     */
    public $realm = 'api';

    /**
     * @var string Authorization header schema, default 'Bearer'
     */
    public $schema = 'Bearer';


    /**
     * {@inheritdoc}
     */
    public function authenticate($user, $request, $response)
    {
        $user = new AuthUser();
        $model = new TokenJwt();
        $authHeader = $request->getHeaders()->get($this->header);
        if ($authHeader !== null && $this->pattern !== null && preg_match($this->pattern, $authHeader, $matches)) {
            $token = $model->loadToken($matches[1]);
            if ($token === null) {
               return null;
            }
            $authField = $model->uniqueToken;
            $tokenKey = $model->user->$authField;
            return AuthUser::findIdentityByAccessToken($tokenKey);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function challenge($response)
    {
        $response->getHeaders()->set(
            'WWW-Authenticate',
            "{$this->schema} realm=\"{$this->realm}\", error=\"invalid_token\", error_description=\"The access token invalid or expired\""
        );
    }
}
