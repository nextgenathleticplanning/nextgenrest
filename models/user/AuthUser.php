<?php
namespace app\models\user;

class AuthUser extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $role;
    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'auth0',
            'password' => 'X@GevT*^KbKXzADBCv&5Rpd$^k3xrPDY5kwQ!XQN4kC5wMq@b9^huHfrAVh39vke$&d3%WPah#9f36thVM@#Kgb!&D9^h$pRUUrM',
            'authKey' => 'E&6y5ZfMZpWV4A6E%ujThPP&XSemJHzDSxtgbgPD*CuAj$bdc6TG@DF3H6CzMGpmvzYrvK6h3ygRt9#hrYMu69uhB@zUpE!*4uze',
            'accessToken' => 'dGx3Aw4FSuMb$8kM*NfdmBkJ@TAKH4ndedbYWGuDvJ#65n&9T!ftWdvQKe85u4Zc!cTzgt58fmAdJq^vsA$Qfjek9r$k6A4yxSsC',
            'role'=> 40
        ],
    ];

    public static function restLogin($username,$password){
        $user = self::findByUsername($username);
        if($user !== null && $user->validatePassword($password)){
            return $user;
        }
        return null;
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }
        return null;
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
