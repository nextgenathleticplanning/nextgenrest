<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $primaryColor
 * @property string $secondaryColor
 * @property string $tertiaryColor
 * @property string $companyEmail
 * @property string $address
 * @property string $street
 * @property string $city
 * @property string $state
 * @property int $zip
 * @property string $phone
 * @property int $email_prefs
 *
 * @property UserClients[] $userClients
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'companyEmail', 'address'], 'required'],
            [['zip', 'email_prefs'], 'integer'],
            [['name', 'logo', 'primaryColor', 'secondaryColor', 'tertiaryColor', 'companyEmail', 'address', 'street', 'city', 'state', 'phone'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['companyEmail'], 'validateEmail'],
            [['address'], 'unique'],
        ];
    }

    public function validateEmail($attribute,$params){
        $userEmailExists = User::find()->where(['email'=>$this->companyEmail])->exists();
        if(User::find()->where(['email'=>$this->companyEmail])->exists()){
            $this->addError('companyEmail','Company Email can\'t be another User Email. A user already has this email registered');
        }else if(Clients::find()->where(['companyEmail'=>$this->companyEmail])->exists()){
            $this->addError('companyEmail','Company Email is already registered');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'logo' => 'Logo',
            'primaryColor' => 'Primary Color',
            'secondaryColor' => 'Secondary Color',
            'tertiaryColor' => 'Tertiary Color',
            'companyEmail' => 'Company Email',
            'address' => 'Address',
            'street' => 'Street',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'phone' => 'Phone',
            'email_prefs' => 'Email Prefs',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClients()
    {
        return $this->hasMany(UserClients::className(), ['client_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if( $insert){
            $client = \Yii::createObject([
                'class' => 'app\models\user\UserClients',
                'user_id' => Yii::$app->user->id,
                'client_id' => $this->id,
            ]);
            $client->save();
        }
    }
}
