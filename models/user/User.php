<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $user_id
 * @property string $email
 * @property string $username
 * @property int $role
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $birth_date
 * @property int $gender
 * @property string $backup_email
 * @property string $street
 * @property string $city
 * @property string $state
 * @property int $zip
 * @property string $phone
 * @property int $email_prefs
 *
 * @property Role $role0
 * @property UserClients[] $userClients
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'email', 'username'], 'required'],
            [['role', 'gender', 'zip', 'email_prefs'], 'integer'],
            [['birth_date'], 'safe'],
            [['user_id', 'email', 'username', 'first_name', 'middle_name', 'last_name', 'backup_email', 'street', 'city', 'state', 'phone'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['email'], 'unique'],
            ['email', 'email','allowName'=>true],
            [['username'], 'unique'],
            [['role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'username' => 'Username',
            'role' => 'Role',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'birth_date' => 'Birth Date',
            'gender' => 'Gender',
            'backup_email' => 'Backup Email',
            'street' => 'Street',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'phone' => 'Phone',
            'email_prefs' => 'Email Prefs',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClients()
    {
        return $this->hasMany(UserClients::className(), ['user_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // Yii::trace($token,'dev');
        return static::findOne(['user_id' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}
