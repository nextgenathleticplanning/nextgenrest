<?php

use app\migrations\Migration;

/**
 * Class m180528_031433_UserTbls
 */
class m180528_031433_UserTbls extends Migration
{
    public function safeUp()
    {
        $time = time();
        // User Table
        $this->createTable('{{%user}}', [
            'id'                    => $this->primaryKey(),
            'username'              => $this->string()->notNull()->unique(),
            'email'                 => $this->string()->notNull()->unique(),
            'status'                => $this->smallInteger()->notNull()->defaultValue(10),
            'role'                  => $this->smallInteger()->notNull()->defaultValue(10),
            'auth_key'              => $this->string(32)->unique()->notNull(),
            'access_token'          => $this->string(32)->unique()->notNull(),
            'password_hash'         => $this->string()->notNull(),
            'confirmation_token'    => $this->string(32)->notNull(),
            'confirmation_sent_at'  => $this->integer(),
            'confirmed_at'          => $this->integer(),
            'recovery_token'        => $this->string(32),
            'recovery_sent_at'      => $this->integer(),
            'blocked_at'            => $this->integer(),
            'role'                  => $this->smallInteger()->notNull()->defaultValue(10),
            'registration_ip'       => $this->string(),
            'created_at'            => $this->integer()->notNull(),
            'updated_at'            => $this->integer()->notNull(),
        ], $this->tableOptions);

        // Unique User Table Indexes
        $this->createIndex('indxUser_unique_username', '{{%user}}', 'username', true);
        $this->createIndex('indxUser_unique_email', '{{%user}}', 'email', true);
        $this->createIndex('indxUser_unique_auth_key', '{{%user}}', 'auth_key', true);
        $this->createIndex('indxUser_unique_access_token', '{{%user}}', 'access_token', true);
        $this->createIndex('indxUser_unique_confirmation', '{{%user}}', 'id, confirmation_token', true);
        $this->createIndex('indxUser_unique_recovery', '{{%user}}', 'id, recovery_token', true);

        // Profile Table
        $this->createTable('{{%profile}}', [
            'user_id'           => $this->integer() . ' PRIMARY KEY',
            'first_name'        => $this->string(),
            'middle_name'       => $this->string(),
            'last_name'         => $this->string(),
            'birth_date'        => $this->date(),
            'gender'            => $this->smallInteger(),
            'backup_email'      => $this->string(),
            'street'            => $this->string(),
            'city'              => $this->string(),
            'state'             => $this->string(),
            'zip'               => $this->integer(),
            'phone'             => $this->string(),
            'email_prefs'       => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at'        => $this->integer()->notNull(),
            'updated_at'        => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex ('indxProfile', '{{%profile}}', [
            'gender',
            'email_prefs',
            'birth_date',
        ],$unique = false);

        $this->addForeignKey('fk_user_profile', '{{%profile}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');


        $this->createTable('{{%session}}', [
            'id'                    => $this->string() . ' PRIMARY KEY',
            'user_id'               => $this->integer(),
            'last_write'            => $this->integer(),
            'expire'                => $this->integer(),
            'data'                  => $this->sessionBlobType,
        ], $this->tableOptions);

        $this->createIndex ('indxSessionUserLogins', '{{%session}}', [
            'user_id',
            'last_write',
            'expire',
        ],$unique = false);
        $this->addForeignKey('fk_user_session', '{{%session}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');


        $this->createTable('{{%role}}', [
            'id'        => $this->smallInteger() . ' PRIMARY KEY',
            'name'      => $this->string() . '(45)',
        ], $this->tableOptions);

        $this->insert('role',[
            'id'=>40,
            'name' =>'SuperUser',
        ]);
        $this->insert('role',[
            'id'=>30,
            'name' =>'Admin',
        ]);
        $this->insert('role',[
            'id'=>20,
            'name' =>'Coach',
        ]);
        $this->insert('role',[
            'id'=>10,
            'name' =>'Standard',
        ]);

        $this->createTable('{{%status}}', [
            'id'        => $this->smallInteger() . ' PRIMARY KEY',
            'name'      => $this->string() . '(45)',
        ], $this->tableOptions);

        $this->insert('status',[
            'id'=>10,
            'name' =>'Active',
        ]);
        $this->insert('status',[
            'id'=>20,
            'name' =>'Inactive',
        ]);

        $this->createTable('{{%gender}}', [
            'id'        => $this->smallInteger() . ' PRIMARY KEY',
            'name'      => $this->string(45),
        ], $this->tableOptions);

        $this->insert('gender',[
            'id'=>10,
            'name' =>'Male',
        ]);

        $this->insert('gender',[
            'id'=>20,
            'name' =>'Female',
        ]);

        $this->addForeignKey('fk_role_user_id', '{{%user}}', 'role', '{{%role}}', 'id');
        $this->addForeignKey('fk_status_user_id', '{{%user}}', 'status', '{{%status}}', 'id');

        $this->createTable('{{%userSettings}}', [
            'id'                    => $this->primaryKey(),
            'user_id'               => $this->integer()->notNull(),
            'key'                   => $this->integer()->notNull(),
            'data'                  => 'LONGTEXT NOT NULL',
            'created_at'            => $this->integer()->notNull(),
            'updated_at'            => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex ('indxUserSettings', '{{%userSettings}}', [
            'user_id',
            'key',
        ],$unique = true);

        $this->addForeignKey('fk_userSettings', '{{%userSettings}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%social_accounts}}', [
            'id'                    => $this->primaryKey(),
            'user_id'               => $this->integer()->notNull(),
            'provider'              => $this->string()->notNull(),
            'client_id'             => $this->string()->notNull(),
            'data'                  => $this->text(),
            'token'                 => $this->text(),
            'secret'                => $this->text(),
            'created_at'            => $this->integer()->notNull(),
            'updated_at'            => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_user_socialAccounts', '{{%social_accounts}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

    }

        public function safeDown()
        {
            $this->dropForeignKey('fk_user_socialAccounts','{{%social_accounts}}');
            $this->dropTable('{{%social_accounts}}');
            $this->dropForeignKey('fk_userSettings', '{{%userSettings}}');
            $this->dropTable('{{%userSettings}}');
            $this->dropForeignKey('fk_status_user_id','{{%user}}');
            $this->dropForeignKey('fk_role_user_id','{{%user}}');
            $this->dropTable('{{%gender}}');
            $this->dropTable('{{%status}}');
            $this->dropTable('{{%role}}');
            $this->dropForeignKey('fk_user_session','{{%session}}');
            $this->dropTable('{{%session}}');
            $this->dropForeignKey('fk_user_profile','{{%profile}}');
            $this->dropTable('{{%profile}}');
            $this->dropTable('{{%user}}');
        }
}
