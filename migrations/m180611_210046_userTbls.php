<?php

use app\migrations\Migration;

/**
 * Class m180611_210046_userTbls
 */
class m180611_210046_userTbls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id'                    => $this->primaryKey(),
            'user_id'               => $this->string()->notNull()->unique(),
            'email'                 => $this->string()->notNull()->unique(),
            'username'              => $this->string()->notNull()->unique(),
            'role'                  => $this->smallInteger()->notNull()->defaultValue(10),
            'first_name'            => $this->string(),
            'middle_name'           => $this->string(),
            'last_name'             => $this->string(),
            'birth_date'            => $this->date(),
            'gender'                => $this->smallInteger(),
            'backup_email'          => $this->string(),
            'street'                => $this->string(),
            'city'                  => $this->string(),
            'state'                 => $this->string(),
            'zip'                   => $this->integer(),
            'phone'                 => $this->string(),
            'email_prefs'           => $this->smallInteger()->notNull()->defaultValue(10),
        ], $this->tableOptions);

        $this->createIndex ('indxUser', '{{%user}}', [
            'user_id',
            'username',
            'gender',
            'role',
            'email_prefs',
            'birth_date',
        ],$unique = false);

        $this->createTable('{{%role}}', [
            'id'        => $this->smallInteger() . ' PRIMARY KEY',
            'name'      => $this->string() . '(45)',
        ], $this->tableOptions);

        $this->insert('role',[
            'id'=>40,
            'name' =>'SuperUser',
        ]);
        $this->insert('role',[
            'id'=>30,
            'name' =>'Admin',
        ]);
        $this->insert('role',[
            'id'=>20,
            'name' =>'Coach',
        ]);
        $this->insert('role',[
            'id'=>10,
            'name' =>'Standard',
        ]);

        $this->createTable('{{%gender}}', [
            'id'        => $this->smallInteger() . ' PRIMARY KEY',
            'name'      => $this->string(45),
        ], $this->tableOptions);

        $this->insert('gender',[
            'id'=>10,
            'name' =>'Male',
        ]);

        $this->insert('gender',[
            'id'=>20,
            'name' =>'Female',
        ]);

        $this->addForeignKey('fk_role_user_id', '{{%user}}', 'role', '{{%role}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk_role_user_id','{{%user}}');
        $this->dropTable('{{%gender}}');
        $this->dropTable('{{%role}}');
        $this->dropTable('{{%user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180611_210046_userTbls cannot be reverted.\n";

        return false;
    }
    */
}
