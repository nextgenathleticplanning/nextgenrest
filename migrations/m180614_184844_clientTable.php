<?php

use app\migrations\Migration;

/**
 * Class m180614_184844_clientTable
 */
class m180614_184844_clientTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%clients}}', [
            'id'                    => $this->primaryKey(),
            'name'                  => $this->string()->notNull()->unique(),
            'logo'                  => $this->string(),
            'primaryColor'          => $this->string(),
            'secondaryColor'        => $this->string(),
            'tertiaryColor'         => $this->string(),
            'companyEmail'          => $this->string()->notNull()->unique(),
            'address'               => $this->string()->notNull()->unique(),
            'address'               => $this->string()->notNull()->unique(),
            'street'                => $this->string(),
            'city'                  => $this->string(),
            'state'                 => $this->string(),
            'zip'                   => $this->integer(),
            'phone'                 => $this->string(),
            'email_prefs'           => $this->smallInteger()->notNull()->defaultValue(10),
        ], $this->tableOptions);

        $this->createTable('{{%userClients}}', [
            'id'                    => $this->primaryKey(),
            'user_id'               => $this->integer()->notNull(),
            'client_id'             => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_client_user_id', '{{%userClients}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_client_client_id', '{{%userClients}}', 'client_id', '{{%clients}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_client_client_id','{{%userClients}}');
        $this->dropForeignKey('fk_client_user_id','{{%userClients}}');
        $this->dropTable('{{%userClients}}');
        $this->dropTable('{{%clients}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180614_184844_clientTable cannot be reverted.\n";

        return false;
    }
    */
}
