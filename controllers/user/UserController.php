<?php
namespace app\controllers\user;

use Yii;
// use yii\rest\Controller;
use yii\rest\ActiveController as Controller;
use app\models\auth\Auth0HttpBearerAuth;
use app\models\auth\TokenHttpBearerAuth;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\data\DataFilter;
use app\models\user\User;
use yii\web\NotFoundHttpException;


class UserController extends Controller{

    public $modelClass = 'app\models\user\User';

    public function behaviors()
    {
        // remove authentication filter
   	    $behaviors = parent::behaviors();
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => null,
                'Access-Control-Max-Age' => 86400,
                'Access-Control-Expose-Headers' => [],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                TokenHttpBearerAuth::className(),
                Auth0HttpBearerAuth::className(),
            ],
            // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
            'except' => ['options']
        ];
        return $behaviors;
    }

	public function actions()
    {
        $actions = parent::actions();
        // unset($actions['index'], $actions['view'],$actions['create'], $actions['update'],$actions['delete'], $actions['options']);
        unset($actions['view']);
        return $actions;
    }

    public function actionGetUser(){
        $requestParams = ArrayHelper::merge(Yii::$app->request->queryParams,Yii::$app->request->bodyParams);
        $id = ArrayHelper::getValue($requestParams,'user_id');
        $model = $this->findModel($id);
    //     if ($this->checkAccess) {
    //        call_user_func($this->checkAccess, $this->id, $model);
    //    }
        return $model;
    }
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model =
            User::find()
                ->where(['user.user_id'=>$id])
                ->joinWith([
                    'userClients' => function($q){
                        $q->joinWith('client');
                    }
                ])
                ->asArray()
                ->one()
            ) !== null) {
            return $model;
        } else {

        throw new NotFoundHttpException("Object not found: $id");
        }
    }
    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param \yii\base\Model $model the model to be accessed. If `null`, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        // if ($action === 'update' || $action === 'delete') {
        //     if ($model->author_id !== \Yii::$app->user->id)
        //         throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s articles that you\'ve created.', $action));
        // }
    }
}
