<?php
namespace app\controllers;

use Yii;
use yii\rest\Controller;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use app\models\user\AuthUser;
use yii\filters\auth\HttpBasicAuth;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use app\models\auth\TokenJwt;

class GenerateTokenController extends Controller
{

    public function behaviors()
	{
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors, [
           'authenticator' => [
               'class' => HttpBasicAuth::className(),
               'auth' => function ($username, $password) {
                   return AuthUser::restLogin($username, $password);
               }
           ]
       ]);
	}


	public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['view'],$actions['create'], $actions['update'],$actions['delete'], $actions['options']);
        return $actions;
    }

	public function actionIndex()
    {
        // return Yii::$app->user;// 'asds sdf';
        $params = ArrayHelper::merge(Yii::$app->request->queryParams,Yii::$app->request->bodyParams);
        $model = new TokenJwt();
        $expirationTime = $model->expirationTime;
        $signer = new Sha256();
        $accessKey = $model->jti;
        $token = $model->builder
            ->issuedBy($model->serverName) // Configures the issuer (iss claim)
            ->canOnlyBeUsedBy($model->iss) // Configures the audience (aud claim)
            ->identifiedBy(Yii::$app->user->identity->$accessKey, true) // Configures the id (jti claim), replicating as a header item
            ->issuedAt(time()) // Configures the time that the token was issue (iat claim)
            ->expiresAt($expirationTime) // Configures the expiration time of the token (exp claim)
            ->with('uid', Yii::$app->user->identity->id) // Configures a new claim, called "uid"
            ->with('username', Yii::$app->user->identity->username) // Configures a new claim, called "uid"
            ->sign($signer, $model->privateKey) // creates a signature using your private key
            ->getToken(); // Retrieves the generated token

        Yii::trace($token->verify($signer, $model->publicKey),'dev');
        return ['jwt'=>$token->__toString(),'expiresAt'=> $expirationTime];
    }

}
