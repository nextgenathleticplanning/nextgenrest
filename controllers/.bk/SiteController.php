<?php
namespace app\controllers;

use Yii;
use yii\rest\Controller;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use app\models\User;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class SiteController extends Controller
{

    public function behaviors()
	{

        return ArrayHelper::merge(parent::behaviors(), [
		    'corsFilter' => [
	            'class' => \yii\filters\Cors::className(),
	             'cors' => [
	                'Origin' => ['*'],
			        'Access-Control-Request-Method' => ['GET', 'POST', 'OPTIONS'],
			        'Access-Control-Request-Headers' => ['*'],
			        'Access-Control-Allow-Credentials' => null,
			        'Access-Control-Max-Age' => 86400,
			        'Access-Control-Expose-Headers' => [],
	            ],
	        ],
        ]);
	}


	public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['view'],$actions['create'], $actions['update'],$actions['delete'], $actions['options']);
        return $actions;
    }

	public function actionIndex()
    {
        $params = ArrayHelper::merge(Yii::$app->request->queryParams,Yii::$app->request->bodyParams);
        return ['success' => 'Hello World'];
    }

}
